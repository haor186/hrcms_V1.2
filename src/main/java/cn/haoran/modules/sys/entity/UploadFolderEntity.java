package cn.haoran.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("sys_upload_folder")
public class UploadFolderEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Long id;
    /**
     *
     */
    private Long parentId;
    /**
     * 文件夹名称
     */
    private String name;

    /**
     * 是否有子节点 0-无 1-有
     */
    private String hasChild;
    /**
     * 上级文件夹路径
     */
    private String parentPath;
    /**
     * 包含文件数
     */
    private Integer fileNum;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 排序序号
     */
    private Integer orderNum;

}

