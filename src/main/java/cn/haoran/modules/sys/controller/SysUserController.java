package cn.haoran.modules.sys.controller;

import cn.haoran.common.utils.Constant;
import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.ResultInfo;
import cn.haoran.common.validator.Assert;
import cn.haoran.common.validator.ValidatorUtils;
import cn.haoran.common.validator.group.AddGroup;
import cn.haoran.common.validator.group.UpdateGroup;
import cn.haoran.modules.sys.entity.SysUserEntity;
import cn.haoran.modules.sys.form.PasswordForm;
import cn.haoran.modules.sys.service.SysUserRoleService;
import cn.haoran.modules.sys.service.SysUserService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Create by hanran on 2019/6/15
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;


    /**
     * 所有用户列表
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:user:list")
    public ResultInfo list(@RequestParam Map<String, Object> params){
        //只有超级管理员，才能查看所有管理员列表
        if(getUserId() != Constant.SUPER_ADMIN){
            params.put("createUserId", getUserId());
        }
        PageUtils page = sysUserService.queryPage(params);

        return ResultInfo.ok().put("page", page);
    }

    /**
     * 获取登录的用户信息
     */
    @GetMapping("/info")
    public ResultInfo info(){
        //return ResultInfo.ok().put("user", getUser());
        return ResultInfo.ok().put("user", sysUserService.queryInfo(getUserId()));
    }

    /**
     * 修改登录用户密码
     */
    //@SysLog("修改密码")
    @PostMapping("/password")
    public ResultInfo password(@RequestBody PasswordForm form){
        Assert.isBlank(form.getNewPassword(), "新密码不为能空");

        //sha256加密
        String password = new Sha256Hash(form.getPassword(), getUser().getSalt()).toHex();
        //sha256加密
        String newPassword = new Sha256Hash(form.getNewPassword(), getUser().getSalt()).toHex();

        //更新密码
        boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
        if(!flag){
            return ResultInfo.error("原密码不正确");
        }

        return ResultInfo.ok();
    }

    /**
     * 用户信息
     */
    @GetMapping("/info/{userId}")
    @RequiresPermissions("sys:user:info")
    public ResultInfo info(@PathVariable("userId") Long userId){
        SysUserEntity user = sysUserService.selectById(userId);

        //获取用户所属的角色列表
        List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
        user.setRoleIdList(roleIdList);

        return ResultInfo.ok().put("user", user);
    }

    /**
     * 保存用户
     */
    //@SysLog("保存用户")
    @PostMapping("/save")
    @RequiresPermissions("sys:user:save")
    public ResultInfo save(@RequestBody SysUserEntity user){
        ValidatorUtils.validateEntity(user, AddGroup.class);

        user.setCreateUserId(getUserId());
        sysUserService.save(user);

        return ResultInfo.ok();
    }

    /**
     * 修改用户
     */
    //@SysLog("修改用户")
    @PostMapping("/update")
    @RequiresPermissions("sys:user:update")
    public ResultInfo update(@RequestBody SysUserEntity user){
        ValidatorUtils.validateEntity(user, UpdateGroup.class);

        user.setCreateUserId(getUserId());
        sysUserService.update(user);

        return ResultInfo.ok();
    }

    /**
     * 删除用户
     */
    //@SysLog("删除用户")
    @PostMapping("/delete")
    @RequiresPermissions("sys:user:delete")
    public ResultInfo delete(@RequestBody Long[] userIds){
        if(ArrayUtils.contains(userIds, 1L)){
            return ResultInfo.error("系统管理员不能删除");
        }

        if(ArrayUtils.contains(userIds, getUserId())){
            return ResultInfo.error("当前用户不能删除");
        }

        sysUserService.deleteBatch(userIds);

        return ResultInfo.ok();
    }
}
