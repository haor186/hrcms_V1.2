package cn.haoran.modules.sys.controller;

import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.ResultInfo;
import cn.haoran.modules.sys.service.SysLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 系统日志
 *
 * Create by hanran on 2019/6/15
 */
@Controller
@RequestMapping("/sys/log")
public class SysLogController {
    @Autowired
    private SysLogService sysLogService;

    /**
     * 列表
     */
    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("sys:log:list")
    public ResultInfo list(@RequestParam Map<String, Object> params){
        PageUtils page = sysLogService.queryPage(params);

        return ResultInfo.ok().put("page", page);
    }

}
