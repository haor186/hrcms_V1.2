package cn.haoran.modules.sys.controller;

import cn.haoran.common.utils.Constant;
import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.ResultInfo;
import cn.haoran.common.validator.ValidatorUtils;
import cn.haoran.modules.sys.entity.SysRoleEntity;
import cn.haoran.modules.sys.service.SysRoleMenuService;
import cn.haoran.modules.sys.service.SysRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色管理
 *
 * Create by hanran on 2019/6/15
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    /**
     * 角色列表
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:role:list")
    public ResultInfo list(@RequestParam Map<String, Object> params){
        //如果不是超级管理员，则只查询自己创建的角色列表
        if(getUserId() != Constant.SUPER_ADMIN){
            params.put("createUserId", getUserId());
        }

        PageUtils page = sysRoleService.queryPage(params);

        return ResultInfo.ok().put("page", page);
    }

    /**
     * 角色列表
     */
    @GetMapping("/select")
    @RequiresPermissions("sys:role:select")
    public ResultInfo select(){
        Map<String, Object> map = new HashMap<>();

        //如果不是超级管理员，则只查询自己所拥有的角色列表
        if(getUserId() != Constant.SUPER_ADMIN){
            map.put("createUserId", getUserId());
        }
        List<SysRoleEntity> list = sysRoleService.selectByMap(map);

        return ResultInfo.ok().put("list", list);
    }

    /**
     * 角色信息
     */
    @GetMapping("/info/{roleId}")
    @RequiresPermissions("sys:role:info")
    public ResultInfo info(@PathVariable("roleId") Long roleId){
        SysRoleEntity role = sysRoleService.selectById(roleId);

        //查询角色对应的菜单
        List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
        role.setMenuIdList(menuIdList);

        return ResultInfo.ok().put("role", role);
    }

    /**
     * 保存角色
     */
    //@SysLog("保存角色")
    @PostMapping("/save")
    @RequiresPermissions("sys:role:save")
    public ResultInfo save(@RequestBody SysRoleEntity role){
        ValidatorUtils.validateEntity(role);

        role.setCreateUserId(getUserId());
        sysRoleService.save(role);

        return ResultInfo.ok();
    }

    /**
     * 修改角色
     */
    //@SysLog("修改角色")
    @PostMapping("/update")
    @RequiresPermissions("sys:role:update")
    public ResultInfo update(@RequestBody SysRoleEntity role){
        ValidatorUtils.validateEntity(role);

        role.setCreateUserId(getUserId());
        sysRoleService.update(role);

        return ResultInfo.ok();
    }

    /**
     * 删除角色
     */
    //@SysLog("删除角色")
    @PostMapping("/delete")
    @RequiresPermissions("sys:role:delete")
    public ResultInfo delete(@RequestBody Long[] roleIds){
        sysRoleService.deleteBatch(roleIds);

        return ResultInfo.ok();
    }
}
