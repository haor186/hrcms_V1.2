package cn.haoran.modules.sys.service;

import cn.haoran.common.utils.PageUtils;
import cn.haoran.modules.sys.entity.SysLogEntity;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * 系统日志
 *
 * Create by hanran on 2019/6/15
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
