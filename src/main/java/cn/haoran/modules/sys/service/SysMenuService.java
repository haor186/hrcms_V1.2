package cn.haoran.modules.sys.service;

import cn.haoran.modules.sys.entity.SysMenuEntity;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * 菜单管理
 *
 * Create by hanran on 2019/6/15
 */
public interface SysMenuService extends IService<SysMenuEntity> {

    /**
     * 根据父菜单，查询子菜单
     * @param parentId 父菜单ID
     * @param menuIdList  用户菜单ID
     */
    List<SysMenuEntity> queryListParentId(Long parentId, List<Long> menuIdList);

    /**
     * 根据父菜单，查询子菜单
     * @param parentId 父菜单ID
     */
    List<SysMenuEntity> queryListParentId(Long parentId);

    /**
     * 获取不包含按钮的菜单列表
     */
    List<SysMenuEntity> queryNotButtonList();

    /**
     * 获取用户菜单列表
     */
    List<SysMenuEntity> getUserMenuList(Long userId);

    /**
     * 删除
     */
    void delete(Long menuId);

    /**
     * 获取菜单关联的资源列表
     * @param parentId
     * @return
     */
    List<SysMenuEntity> listResourcesByParentId(Long parentId);
}
