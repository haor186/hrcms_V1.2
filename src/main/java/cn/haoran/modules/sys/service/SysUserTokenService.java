package cn.haoran.modules.sys.service;

import cn.haoran.common.utils.ResultInfo;
import cn.haoran.modules.sys.entity.SysUserTokenEntity;
import com.baomidou.mybatisplus.service.IService;

/**
 * 用户Token
 *
 * Create by hanran on 2019/6/15
 */
public interface SysUserTokenService extends IService<SysUserTokenEntity> {

    /**
     * 生成token
     * @param userId  用户ID
     */
    ResultInfo createToken(long userId);

    /**
     * 退出，修改token值
     * @param userId  用户ID
     */
    void logout(long userId);

}
