package cn.haoran.modules.sys.service;

import cn.haoran.modules.sys.entity.SysRoleMenuEntity;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 * Create by hanran on 2019/6/15
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {

    void saveOrUpdate(Long roleId, List<Long> menuIdList);

    /**
     * 根据角色ID，获取菜单ID列表
     */
    List<Long> queryMenuIdList(Long roleId);

    /**
     * 根据角色ID数组，批量删除
     */
    int deleteBatch(Long[] roleIds);

}
