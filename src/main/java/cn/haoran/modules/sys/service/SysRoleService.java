package cn.haoran.modules.sys.service;

import cn.haoran.common.utils.PageUtils;
import cn.haoran.modules.sys.entity.SysRoleEntity;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 角色
 *
 * Create by hanran on 2019/6/15
 */
public interface SysRoleService extends IService<SysRoleEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void save(SysRoleEntity role);

    /**
     * 保存（临时，前端树结构不支持半选节点选中状态，所以在后端实现选取）
     * @param role
     */
    void saveByTemp(SysRoleEntity role);

    void update(SysRoleEntity role);

    void deleteBatch(Long[] roleIds);


    /**
     * 查询用户创建的角色ID列表
     */
    List<Long> queryRoleIdList(Long createUserId);
}
