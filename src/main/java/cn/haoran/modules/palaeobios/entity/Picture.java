package cn.haoran.modules.palaeobios.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * 图片的实体类
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BizPicture对象", description="")
@TableName("biz_picture")
public class Picture implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId
    private Long id;
    @ApiModelProperty(value = "文章id")
    @NotEmpty
    private Long articleId;

    @ApiModelProperty(value = "图片存储路径")
    @Length(max = 250)
    @NotEmpty
    private String url;

    @ApiModelProperty(value = "图片描述")
    private String describe;


}
