package cn.haoran.modules.palaeobios.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 栏目的实体类
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BizColumn对象", description="")
@TableName("biz_column")
public class Column implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId
    private Long id;
    @ApiModelProperty(value = "父级id")
    private Long parentId;

    @ApiModelProperty(value = "栏目名称")
    @Length(max = 40)
    @NotEmpty
    private String name;

    private LocalDateTime createDate;

    private String createName;

    private Long createId;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer delete = IDeteleState.NOT_DELETE;
}
