package cn.haoran.modules.palaeobios.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document(collection="log")
public class LogEntity {
    private String createDateTime;
    /* 日志创建时间 */
    private String createDate;
    /*  执行类型 */
    private String methodName;
    /* 内容 */
    private String content;
    /*  修改者Ip */
    private String ip;
    /* 操作者 */
    private String name;
}
