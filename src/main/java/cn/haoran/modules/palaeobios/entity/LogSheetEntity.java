package cn.haoran.modules.palaeobios.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * 用户浏览记录
 */
@Document(collection="log_sheet")
@Data
public class LogSheetEntity {
    /* IP地址 */
    private String ip;
    /* 浏览时间 */
    private String createDate;
    /* 浏览日期 */
    private String createDateTime;
    /* 文章id */
    private String aId;
    /* 标题 */
    private String title;
    /* 栏目id */
    private String pId;

    /* 用于分组的时间 */
    private LocalDateTime date;
    /* 统计 */
    private Integer allCount;
    /* 时间戳 */
    private Long timestamp;
}
