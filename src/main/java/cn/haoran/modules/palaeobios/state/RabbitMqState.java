package cn.haoran.modules.palaeobios.state;

/**
 * 定义文章写队列的常量
 */
public interface RabbitMqState {
    // 文章增加
    String insert = "cms.inster";

    // 文章删除
    String delete = "cms.delete";

    // 文章修改
    String update = "cms.update";
}
