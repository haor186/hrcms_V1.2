package cn.haoran.modules.palaeobios.utils;

import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * 首页栏目id 不能删除
 */
@Component
public class IndexColumn {
    private HashMap<Long,String> indexColumnId;
    public IndexColumn(){
        indexColumnId = new HashMap<>();
        indexColumnId.put(4L,"头条新闻");
        indexColumnId.put(5L,"综合新闻");
        indexColumnId.put(6L,"图片新闻");
        indexColumnId.put(7L,"科研进展");
        indexColumnId.put(26L,"科学传播");
    }

    public String get(Long id){
        return indexColumnId.get(id);
    }

}
