package cn.haoran.modules.palaeobios.mapper;


import cn.haoran.modules.palaeobios.entity.Article;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 获得文章的所有id 用于做布隆映射
     * @return
     */
    @Select("SELECT id FROM biz_article ")
    List<Long> findAllId();
}
