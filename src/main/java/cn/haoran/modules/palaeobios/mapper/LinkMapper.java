package cn.haoran.modules.palaeobios.mapper;

import cn.haoran.modules.palaeobios.entity.Link;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 外部链接对象的mapper
 */
@Mapper
public interface LinkMapper extends BaseMapper<Link> {
}
