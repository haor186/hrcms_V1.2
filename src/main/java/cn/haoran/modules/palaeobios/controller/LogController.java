package cn.haoran.modules.palaeobios.controller;

import cn.haoran.common.utils.ResultInfo;
import cn.haoran.modules.palaeobios.entity.LogEntity;
import cn.haoran.modules.palaeobios.service.LogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/log")
@Api("日志接口列表")
public class LogController {
    @Autowired
    private LogService logService;
    @GetMapping("list")
    @ApiOperation("获得日志列表")
    public ResultInfo list(String date){
        List list = null;
        if (StringUtils.isNotBlank(date)){
            list = logService.findAllByDate(date);
        }else{
            list = logService.findAll();
        }
        return ResultInfo.ok().put("list",list);
    }
}
