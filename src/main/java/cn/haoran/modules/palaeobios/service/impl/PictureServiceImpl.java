package cn.haoran.modules.palaeobios.service.impl;

import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.Query;
import cn.haoran.modules.palaeobios.entity.Picture;
import cn.haoran.modules.palaeobios.mapper.PictureMapper;
import cn.haoran.modules.palaeobios.service.PictureService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
@Service
public class PictureServiceImpl extends ServiceImpl<PictureMapper, Picture> implements PictureService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("name");
        Long articleId = Long.parseLong(params.get("articleId").toString());
        Page<Picture> page = this.selectPage(
                new Query<Picture>(params).getPage(),
                new EntityWrapper<Picture>().eq("article_id",articleId).like(StringUtils.isNotBlank(name),"name",name));
        return new PageUtils(page);
    }
}
