package cn.haoran.modules.palaeobios.service;


import cn.haoran.common.utils.PageUtils;
import cn.haoran.modules.palaeobios.entity.Article;
import com.baomidou.mybatisplus.service.IService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
public interface ArticleService extends IService<Article> {
    /**
     * 分页
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);
    /* 按id查询文章id */
    Article getArticleById(Long id);
    /* 修改操作 */
    void updateArticleById(Article article);
    /* 删除 */
    void deleteArticle(Long[] ids);
    /* 新增 */
    void addArticle(Article article);
    /* 同步es数据库 */
    void synchronizationArticle();
    /* 全文检索关键字的文章  检索条件:文章的标题 内容 基于es */
    List<Article> searchArticleByContent(Map<String,Object> map) throws IOException;

    List<Long> getAllId();
}
