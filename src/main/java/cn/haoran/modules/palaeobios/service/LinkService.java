package cn.haoran.modules.palaeobios.service;

import cn.haoran.common.utils.PageUtils;
import cn.haoran.modules.palaeobios.entity.Link;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

public interface LinkService extends IService<Link> {
    PageUtils queryPage(Map<String, Object> params);
}
