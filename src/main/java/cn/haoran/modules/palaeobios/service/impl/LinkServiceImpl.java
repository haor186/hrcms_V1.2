package cn.haoran.modules.palaeobios.service.impl;

import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.Query;
import cn.haoran.modules.palaeobios.entity.Article;
import cn.haoran.modules.palaeobios.entity.Link;
import cn.haoran.modules.palaeobios.entity.Picture;
import cn.haoran.modules.palaeobios.mapper.ArticleMapper;
import cn.haoran.modules.palaeobios.mapper.LinkMapper;
import cn.haoran.modules.palaeobios.service.LinkService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class LinkServiceImpl extends ServiceImpl<LinkMapper, Link> implements LinkService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("title");
        Page<Link> page = this.selectPage(
                new Query<Link>(params).getPage(),
                new EntityWrapper<Link>().like(StringUtils.isNotBlank(name),"title",name));
        return new PageUtils(page);
    }
}
