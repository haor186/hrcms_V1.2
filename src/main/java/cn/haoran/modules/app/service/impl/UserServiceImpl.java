package cn.haoran.modules.app.service.impl;

import cn.haoran.common.exception.AppException;
import cn.haoran.common.validator.Assert;
import cn.haoran.modules.app.dao.UserDao;
import cn.haoran.modules.app.entity.UserEntity;
import cn.haoran.modules.app.form.LoginForm;
import cn.haoran.modules.app.service.UserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

/**
 * Create by hanran on 2019/6/15
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Override
    public UserEntity queryByMobile(String mobile) {
        UserEntity userEntity = new UserEntity();
        userEntity.setMobile(mobile);
        return baseMapper.selectOne(userEntity);
    }

    @Override
    public long login(LoginForm form) {
        UserEntity user = queryByMobile(form.getMobile());
        Assert.isNull(user, "手机号或密码错误");

        //密码错误
        if(!user.getPassword().equals(DigestUtils.sha256Hex(form.getPassword()))){
            throw new AppException("手机号或密码错误");
        }

        return user.getUserId();
    }
}
