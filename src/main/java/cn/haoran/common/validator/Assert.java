package cn.haoran.common.validator;

import cn.haoran.common.exception.AppException;
import org.apache.commons.lang.StringUtils;

/**
 * 数据校验
 *
 * Create by hanran on 2019/6/15
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new AppException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new AppException(message);
        }
    }
}
