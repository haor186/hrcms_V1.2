package cn.haoran.common.utils;

import cn.haoran.common.exception.AppException;
import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;

/**
 * Create by hanran on 2019/7/24
 */
public class FreemarkerUtils {

    /**
     * 根据ftl模板生成word或excel文件，用于将导出文件存在服务器上
     * @param dataMap word中需要展示的动态数据，用map集合来保存
     * @param templateName word模板名称，例如：test.ftl
     * @param filePath 文件生成的目标路径，例如：D:/wordFile/
     * @param fileName 生成的文件名称，例如：test.doc
     */
    public static void createWordDoc(Map dataMap, String templateName, String filePath, String fileName) {
        try {
            //创建配置实例
            Configuration configuration = new Configuration(Configuration.VERSION_2_3_27);

            //设置编码
            configuration.setDefaultEncoding("UTF-8");

            //ftl模板文件
            configuration.setClassForTemplateLoading(FreemarkerUtils.class,"/ftl/");

            //获取模板
            Template template = configuration.getTemplate(templateName);

            //输出文件
            File outFile = new File(filePath+File.separator+fileName);

            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }

            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));

            //生成文件
            template.process(dataMap, out);

            //关闭流
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 基于freemarker ftl创建文件（word、excel、pdf），导出文件在内存中
     * @param dataMap 文档数据
     * @param templateName 模板名称
     * @param fileName 文件名
     * @return
     * @throws Exception
     */
    public static File createDoc(Map dataMap, String templateName, String fileName) throws Exception {
        File file = null;
        Writer out = null;
        try{
            //创建配置实例
            Configuration configuration = new Configuration(Configuration.VERSION_2_3_27);

            //设置编码
            configuration.setDefaultEncoding("UTF-8");

            //ftl模板文件
            configuration.setClassForTemplateLoading(FreemarkerUtils.class,"/ftl/");

            //获取模板
            Template template = configuration.getTemplate(templateName);
            file = new File(fileName);
            out = new OutputStreamWriter(new FileOutputStream(file), "utf-8");
            template.process(dataMap, out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //关闭流
            if(out != null){
                out.close();
            }
        }
        return file;
    }

    /**
     * 导出文件
     * @param tempFile 工具生成的临时文件（可以是freemarker或是其他模板工具生成）
     * @param response
     * @throws Exception
     */
    public static void exportFile(File tempFile, HttpServletResponse response) throws Exception{
        // content-type类型
        response.setHeader("content-Type", "application/msword");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=xx.doc");

        InputStream fin = null;
        ServletOutputStream out = null;
        try{
            fin = new FileInputStream(tempFile);
            out = response.getOutputStream();
            byte[] buffer = new byte[512];  // 缓冲区
            int bytesToRead = -1;
            while((bytesToRead = fin.read(buffer)) != -1) {
                //文件写入到输出流
                out.write(buffer, 0, bytesToRead);
            }
        }catch (Exception e){
            throw new Exception("导出异常：" + e);
        }finally {
            if(fin != null){
                fin.close();
            }
            if(out != null){
                out.close();
            }
            if(tempFile != null){
                // 删除临时文件
                tempFile.delete();
            }
        }
    }
}
