package cn.haoran.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;
import java.util.Map;

/**
 * mongoDb 查询工具
 * 查询查询需求必备
 *
 */
public class MongoDbUtils {
    @Autowired
    MongoTemplate mongoTemplate;
    private Object companyName;
    private Object startTime;
    private Object endTime;
    Aggregation agg = Aggregation.newAggregation(
            // 第一步：挑选所需的字段，类似select *，*所代表的字段内容
            Aggregation.project("licensePlate", "companyName", "deviceCode", "diverName", "fleet", "lineNumber",
                    "imgUrl", "videoUrl", "ptLoc", "locations"),
            // 第二步：sql where 语句筛选符合条件的记录
            Aggregation.match(
                    Criteria.where("companyName").is(companyName)
                            .and("addedDate").gte(startTime).lte(endTime)),
            // 第三步：分组条件，设置分组字段
            Aggregation.group("companyName", "licensePlate")
                    .count().as("allCount")// 增加COUNT为分组后输出的字段
                    .last("deviceCode").as("deviceCode").last("diverName").as("diverName").last("fleet").as("fleet")
                    .last("lineNumber").as("lineNumber").last("imgUrl").as("imgUrl").last("videoUrl").as("videoUrl")
                    .last("ptLoc").as("ptLoc").last("locations").as("locations"), // 增加publishDate为分组后输出的字段
            // 第四步：重新挑选字段
            Aggregation.project("diverName", "licensePlate", "companyName", "deviceCode", "allCount", "fleet",
                    "lineNumber", "imgUrl", "videoUrl", "ptLoc", "locations")
    );
    AggregationResults<Map> results = mongoTemplate.aggregate(agg, "Historys", Map.class);
    List<Map> list = results.getMappedResults();
}
