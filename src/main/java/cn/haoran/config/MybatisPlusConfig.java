package cn.haoran.config;

import com.baomidou.mybatisplus.MybatisMapWrapperFactory;
import com.baomidou.mybatisplus.mapper.ISqlInjector;
import com.baomidou.mybatisplus.mapper.LogicSqlInjector;
import com.baomidou.mybatisplus.plugins.CachePaginationInterceptor;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.parser.ISqlParser;
import com.baomidou.mybatisplus.spring.boot.starter.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * mybatis-plus配置
 *
 * Create by hanran on 2019/6/15
 */
@Configuration
public class MybatisPlusConfig {
    /**
     * 分页插件
     */
   /* @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        List<ISqlParser> sqlParserList = new ArrayList<>();

        // 攻击 SQL 阻断解析器、加入解析链
        sqlParserList.add(new BlockAttackSqlParser());
        paginationInterceptor.setSqlParserList(sqlParserList);

        return paginationInterceptor;
    }*/
    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
    /* 当然如果你是一个极端分子，请使用 CachePaginationInterceptor 替换默认分页，这样支持分页缓存。 */
        return new CachePaginationInterceptor();
    }
    /**
     * 逻辑删除配置
     * @return mp逻辑删除组件
     */
    @Bean
    public ISqlInjector sqlInjector() {
        return new LogicSqlInjector();
    }

    /**
     * 实体类如果为map对象 key名从下划线自动转驼峰
     * @return
     */
    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return i -> i.setObjectWrapperFactory(new MybatisMapWrapperFactory());
    }
}
